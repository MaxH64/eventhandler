﻿using Contronics.Abstractions;
using Contronics.AdamPLC;
using Contronics.Data;
using FakeItEasy;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;

namespace Contronics.EventHandler.Tests.EventHandlerTests
{
    class TrafficLightTargetTests
    {
        private ITrafficLight _trafficLight;
        private TrafficLightTarget _trafficLightTarget;

        [SetUp]
        public void Setup()
        {
            _trafficLight = A.Fake<ITrafficLight>();
            _trafficLightTarget = new TrafficLightTarget(_trafficLight);
        }

        [Test]
        public void SendAlert_HasColours()
        {
            //Arrange
            var newEvent = CreateEventItem(4, DateTime.Now, CreateListOfAlarmsWithColours(1));

            //Act
            var result = _trafficLightTarget.send(newEvent, A.Fake<EventItem>());

            //Assert
            Assert.That(result == true);
        }

        [Test]
        public void SendAlert_HasNoColours()
        {
            //Arrange
            var newEvent = CreateEventItem(4, DateTime.Now, CreateListOfAlarmsWithOutColours(1));

            //Act
            var result = _trafficLightTarget.send(newEvent, A.Fake<EventItem>());

            //Assert
            Assert.That(result == false);
        }

        [Test]
        public void SendAlert_Colours_setRed_High()
        {
            //Arrange
            var newEvent = CreateEventItem(5, DateTime.Now, CreateListOfAlarmsWithColours(1));

            //Act
            var result = _trafficLightTarget.send(newEvent, A.Fake<EventItem>());

            //Assert
            A.CallTo(() => _trafficLight.Set(A<IPEndPoint>.Ignored, TrafficLightColours.RedLight)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void SendAlert_Colours_setRed_Low()
        {
            //Arrange
            var newEvent = CreateEventItem(1, DateTime.Now, CreateListOfAlarmsWithColours(1));

            //Act
            var result = _trafficLightTarget.send(newEvent, A.Fake<EventItem>());

            //Assert
            A.CallTo(() => _trafficLight.Set(A<IPEndPoint>.Ignored, TrafficLightColours.RedLight)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void SendAlert_Colours_setAmber_High()
        {
            //Arrange
            var newEvent = CreateEventItem(4, DateTime.Now, CreateListOfAlarmsWithColours(1));

            //Act
            var result = _trafficLightTarget.send(newEvent, A.Fake<EventItem>());

            //Assert
            A.CallTo(() => _trafficLight.Set(A<IPEndPoint>.Ignored, TrafficLightColours.RedLight)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void SendAlert_Colours_setAmber_Low()
        {
            //Arrange
            var newEvent = CreateEventItem(2, DateTime.Now, CreateListOfAlarmsWithColours(1));

            //Act
            var result = _trafficLightTarget.send(newEvent, A.Fake<EventItem>());

            //Assert
            A.CallTo(() => _trafficLight.Set(A<IPEndPoint>.Ignored, TrafficLightColours.RedLight)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void SendAlert_Colours_setGreen()
        {
            //Arrange
            var newEvent = CreateEventItem(3, DateTime.Now, CreateListOfAlarmsWithColours(1));

            //Act
            var result = _trafficLightTarget.send(newEvent, A.Fake<EventItem>());

            //Assert
            A.CallTo(() => _trafficLight.Set(A<IPEndPoint>.Ignored, TrafficLightColours.GreenLight)).MustHaveHappenedOnceExactly();
        }

        public EventItem CreateEventItem(int value, DateTime dateTime, List<Alarm> alarms)
        {
             return new EventItem() { AlarmList = alarms, Value = value, EndPointId = 1, DateTime = dateTime, SensorNumber = 1, ChannelId = 1, IpAddress = "192.10.10.00", PrimaryPort = 500, SensorId = 1 };
        }

        public List<Alarm> CreateListOfAlarmsWithColours(int SenserNumber)
        {
            var AlarmList = new List<Alarm>();

            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 5, Type = AlarmType.AlarmIfHigher, Level = AlarmLevel.Critical, Externalcontact = AlarmExternalContact.TrafficLight });
            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 4, Type = AlarmType.AlarmIfHigher, Level = AlarmLevel.Warning, Externalcontact = AlarmExternalContact.TrafficLight });
            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 2, Type = AlarmType.AlarmIfLower, Level = AlarmLevel.Warning, Externalcontact = AlarmExternalContact.TrafficLight });
            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 1, Type = AlarmType.AlarmIfLower, Level = AlarmLevel.Critical, Externalcontact = AlarmExternalContact.TrafficLight });

            return AlarmList;
        }

        public List<Alarm> CreateListOfAlarmsWithOutColours(int SenserNumber)
        {
            var AlarmList = new List<Alarm>();

            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 5, Type = AlarmType.AlarmIfHigher, Level = AlarmLevel.Critical, Externalcontact = AlarmExternalContact.Relay1 });
            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 4, Type = AlarmType.AlarmIfHigher, Level = AlarmLevel.Warning, Externalcontact = AlarmExternalContact.Relay3 });
            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 2, Type = AlarmType.AlarmIfLower, Level = AlarmLevel.Warning });
            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 1, Type = AlarmType.AlarmIfLower, Level = AlarmLevel.Critical, Externalcontact = AlarmExternalContact.Relay2 });

            return AlarmList;
        }
    }
}
