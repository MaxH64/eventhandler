﻿using Contronics.Data;
using FakeItEasy;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contronics.EventHandler.Tests.EventHandlerTests
{

    public class EventServiceTests
    {
        private IMapperStrategy _fakeMapperStrategy;
        private IBaseDataRequests _fakebaseDataRequest;
        private IEnumerable<IEventTarget> _fakeEventTargets;

        [SetUp]
        public void Setup()
        {
            _fakeMapperStrategy = A.Fake<IMapperStrategy>();
            _fakebaseDataRequest = A.Fake<IBaseDataRequests>();
            _fakeEventTargets = A.Fake<IEnumerable<IEventTarget>>();

            A.CallTo(() => _fakebaseDataRequest.GetFilteredResults<EventItem, EventItem>(A<EventItem>.Ignored)).Returns(ListOfChannelEventItems());
            A.CallTo(() => _fakeMapperStrategy.GetMapper<EventItem>()).Returns(_fakebaseDataRequest);
        }

        public EventService EventHandlerConstrutor()
        {
            return new EventService(A.Fake<ILoggerFactory>(), _fakeMapperStrategy, _fakeEventTargets);
        }

        [Test]
        public void AddAlarmsAndValue_ReturnsDictionary()
        {
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            //Act
            var result = EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 1, false);

            //Assert
            Assert.That(result.GetType() == typeof(Dictionary<int, List<EventItem>>));
        }

        [Test]
        public void AddAlarmsAndValue_ValidData_AddsRow()
        {
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            //Act
            var result = EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 1, false);

            //Assert
            Assert.That(result.Count == 1);
        }

        [Test]
        public void AddAlarmsAndValue_InvalidData_NoRow()
        {            
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            //Act
            var result = EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 1, true);

            //Assert
            Assert.That(result.Count == 0);
        }

        [Test]
        public void AddAlarmsAndValue_AddMultipleSameSenser()
        {
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 1, false);
            EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 1, false);
            EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 1, false);

            //Act
            var result = EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 1, false);

            //Assert
            Assert.That(result.Count == 1 && result[1].Count == 4);
        }

        [Test]
        public void AddAlarmsAndValue_AddMultipleDiffrentSenser()
        {
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            
            EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 2, false);
            EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 1, false);
            EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 3, false);

            //Act
            var result = EventHandler.AddAlarmsAndValue(A.Fake<List<Alarm>>(), 1, 1, DateTime.Now, 4, false);

            //Assert
            Assert.That(result.Count == 4 && result[1].Count == 1 && result[2].Count == 1 && result[3].Count == 1 && result[4].Count == 1);
        }

        [Test]
        public void EndResults_AddingValidValueNumber()
        {
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            var dateTime = DateTime.Now;

            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 3, 1, dateTime.AddMinutes(-(3)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 3, 1, dateTime.AddMinutes(-(2)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 3, 1, dateTime.AddMinutes(-(1)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 3, 1, dateTime, 1, false);

            //Act
            var results = EventHandler.EndResults();
            //Assert
            Assert.That(results.Count == 1);
        }

        [Test]
        public void EndResults_AddingInValidValueNumber_lowWarning()
        {
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            var dateTime = DateTime.Now;
            var interval = 1;
            
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 2, 1, dateTime.AddMinutes(-(interval * 3)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 2, 1, dateTime.AddMinutes(-(interval * 2)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 2, 1, dateTime.AddMinutes(-(interval * 1)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 2, 1, dateTime, 1, false);

            //Act
            var results = EventHandler.EndResults();
            //Assert
            Assert.That(results.Count == 1);
        }

        [Test]
        public void EndResults_AddingInValidValueNumber_HighCritical()
        {
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            var dateTime = DateTime.Now;
            var interval = 1;
            //Act
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 2, 5, dateTime.AddMinutes(-(interval * 3)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 2, 6, dateTime.AddMinutes(-(interval * 2)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 2, 7, dateTime.AddMinutes(-(interval * 1)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 2, 6, dateTime, 1, false);

            //Act
            var results = EventHandler.EndResults();
            //Assert
            Assert.That(results.Count == 1);
        }

        [Test]
        public void EndResults_AddingInValidValueNumber_HighCritical_ReturnGood()
        {
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            var dateTime = DateTime.Now;
            var interval = 1;
            //Act
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 5, 2, dateTime.AddMinutes(-(interval * 4)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 3, 2, dateTime.AddMinutes(-(interval * 3)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 5, 2, dateTime.AddMinutes(-(interval * 2)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 5, 2, dateTime.AddMinutes(-(interval * 1)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 3, 2, dateTime, 1, false);

            //Act
            var results = EventHandler.EndResults();
            //Assert
            Assert.That(results.Count == 4);
        }

        [Test]
        public void EndResults_AddingInValidValueNumber_MutliCritrea()
        {
            //Arrange
            var EventHandler = EventHandlerConstrutor();

            var dateTime = DateTime.Now;
            var interval = 1;

            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 3, 2, dateTime.AddMinutes(-(interval * 9)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 3, 2, dateTime.AddMinutes(-(interval * 8)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 4, 2, dateTime.AddMinutes(-(interval * 7)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 5, 2, dateTime.AddMinutes(-(interval * 6)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 5, 2, dateTime.AddMinutes(-(interval * 5)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 4, 2, dateTime.AddMinutes(-(interval * 4)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 2, 2, dateTime.AddMinutes(-(interval * 3)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 1, 2, dateTime.AddMinutes(-(interval * 2)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 1, 2, dateTime.AddMinutes(-(interval * 1)), 1, false);
            EventHandler.AddAlarmsAndValue(CreateListOfAlarms(1), 3, 2, dateTime, 1, false);

            //act
            var results = EventHandler.EndResults();
            //Assert
            Assert.That(results.Count == 7);
        }

        public List<Alarm> CreateListOfAlarms(int SenserNumber)
        {
            var AlarmList = new List<Alarm>();

            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 5, Type = AlarmType.AlarmIfHigher, Level = AlarmLevel.Critical });
            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 4, Type = AlarmType.AlarmIfHigher, Level = AlarmLevel.Warning });
            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 2, Type = AlarmType.AlarmIfLower, Level = AlarmLevel.Warning });
            AlarmList.Add(new Alarm() { SensorNumber = SenserNumber, Value = 1, Type = AlarmType.AlarmIfLower, Level = AlarmLevel.Critical });

            return AlarmList;
        }

        public List<EventItem> ListOfChannelEventItems()
        {
            var listOfChannelEventItems = new List<EventItem>();
            listOfChannelEventItems.Add(new EventItem() { ChannelId = 1, IpAddress = "192.10.10.00", PrimaryPort = 500, SensorId = 1, SensorNumber = 1 });
            listOfChannelEventItems.Add(new EventItem() { ChannelId = 1, IpAddress = "192.10.10.00", PrimaryPort = 500, SensorId = 1, SensorNumber = 2 });
            listOfChannelEventItems.Add(new EventItem() { ChannelId = 1, IpAddress = "192.10.10.00", PrimaryPort = 500, SensorId = 1, SensorNumber = 3 });
            listOfChannelEventItems.Add(new EventItem() { ChannelId = 1, IpAddress = "192.10.10.00", PrimaryPort = 500, SensorId = 1, SensorNumber = 4 });
            return listOfChannelEventItems;
        }
    }
}
