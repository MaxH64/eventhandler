﻿using Contronics.Data;
using System.Collections.Generic;

namespace Contronics.EventHandler
{
    /// <summary>
    /// Interfact for Event Targets
    /// </summary>
    public interface IEventTarget
    {
        /// <summary>
        /// process the new event and send the alert
        /// </summary>
        /// <param name="newEvent">The new event you would like to fire</param>
        /// <param name="lastEvent">The previous event fired</param>
        /// <returns>Returns Bool to determin if this fired</returns>
        bool send(EventItem newEvent, EventItem lastEvent);
    }
}
