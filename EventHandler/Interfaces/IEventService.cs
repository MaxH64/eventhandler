﻿using System;
using System.Collections.Generic;
using Contronics.Data;

namespace Contronics.EventHandler
{
    /// <summary>
    /// Interface for Event Services
    /// </summary>
    public interface IEventService : IDisposable
    {
        /// <summary>
        /// Add Alarms and Values to create a EventItem
        /// </summary>
        /// <param name="alarms">The Alarms relivent to the event</param>
        /// <param name="value">The Value of the result for this event</param>
        /// <param name="endPointId">The EndPointID valid for this event</param>
        /// <param name="dateTime">The DateTime of this event being raised</param>
        /// <param name="sensorNumber">The Sensor Number of this event</param>
        /// <param name="NoData">Wether this contains valid data</param>
        /// <param name="LogInterval">What the Log Interval of this event is</param>
        /// <returns>Dictionary of alarms added</returns>
        Dictionary<int, List<EventItem>> AddAlarmsAndValue(List<Alarm> alarms, int value, int endPointId, DateTime dateTime, int sensorNumber, bool NoData);

        /// <summary>
        /// Fires the end Result method that will process all the alerts
        /// </summary>
        /// <returns>List of fired events</returns>
        List<EventItem> EndResults();
    }
}