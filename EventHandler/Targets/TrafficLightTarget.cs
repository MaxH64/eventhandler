﻿using System.Collections.Generic;
using System.Linq;
using Contronics.Abstractions;
using Contronics.AdamPLC;
using Contronics.Data;

namespace Contronics.EventHandler
{
    /// <summary>
    /// Traffic Light Target Handler
    /// </summary>
    public class TrafficLightTarget : IEventTarget
    {
        #region Private Members

        /// <summary>
        /// TrafficLight Interface 
        /// </summary>
        private ITrafficLight _trafficLight;

        /// <summary>
        /// Determs what light to turn on Kept open to allow for more customisation when needed
        /// </summary>
        /// <param name="alarms">what Alarms are flagging</param>
        /// <returns>The colour to turn the light too</returns>
        private TrafficLightColours rules(List<Alarm> alarms)
        {
            var critical = alarms.FirstOrDefault(o => o.Level == AlarmLevel.Critical);
            var warning = alarms.FirstOrDefault(o => o.Level == AlarmLevel.Warning);
            if (critical != null && critical.Externalcontact == AlarmExternalContact.TrafficLight)
            {
                return TrafficLightColours.RedLight ;
            }
            else if(warning != null && warning.Externalcontact == AlarmExternalContact.TrafficLight)
            {
                return TrafficLightColours.RedLight;
            }
            else
            {
                return TrafficLightColours.GreenLight;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// constructor for TrafficLightTargets
        /// </summary>
        /// <param name="trafficLight">The trafficLight Interface</param>
        public TrafficLightTarget(ITrafficLight trafficLight)
        {
            _trafficLight = trafficLight;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// process the new event and send the alert
        /// </summary>
        /// <param name="newEvent">The new event you would like to fire</param>
        /// <param name="lastEvent">The previous event fired</param>
        /// <returns>Returns Bool to determin if this fired</returns>
        public bool send(EventItem newEvent, EventItem lastEvent)
        {
            if (newEvent.AlarmList.Where(o => o.Externalcontact == AlarmExternalContact.TrafficLight).ToList().Count > 0)
            {
                newEvent.PrimaryPort = 503;
                _trafficLight.Set(newEvent.IpEndPoint, rules(newEvent.AlarmList.Where(o => newEvent.triggeredRules(o)).ToList()));

                return true;
            }
            return false;
        }

        #endregion
    }
}
