﻿using Contronics.Abstractions;
using Contronics.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contronics.EventHandler
{
    /// <summary>
    /// Event Service Handler
    /// </summary>
    public class EventService : IEventService
    {

        #region Private Members

        /// <summary>
        /// The Logger Interface
        /// </summary>
        private ILogger _logger;

        private IMapperStrategy _database;

        /// <summary>
        /// The Event Target list of interfaces
        /// </summary>
        private List<IEventTarget> _eventTargets;

        /// <summary>
        /// List Of all events added before disposed sorted via Sensor
        /// </summary>
        private Dictionary<int, List<EventItem>> _eventItemDictionary = new Dictionary<int, List<EventItem>>();

        /// <summary>
        /// This is the Last Event Fired sorted Via Sensor Number
        /// </summary>
        private Dictionary<int, EventItem> _lastEvent = new Dictionary<int, EventItem>();

        #endregion

        #region Constructor

        /// <summary>
        /// Event Service Constuctor
        /// </summary>
        /// <param name="loggerFactory">The Logger Factory Interface</param>
        /// <param name="database">The Database Interface</param>
        /// <param name="eventTargets">The EventTarget IEnumerable Interface</param>
        public EventService(ILoggerFactory loggerFactory, IMapperStrategy database, IEnumerable<IEventTarget> eventTargets)
        {
            _logger = loggerFactory.CreateLogger(GetType().Name);
            _database = database;
            _eventTargets = eventTargets.ToList();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Add Alarms and Values to create a EventItem
        /// </summary>
        /// <param name="alarms">The Alarms relivent to the event</param>
        /// <param name="value">The Value of the result for this event</param>
        /// <param name="endPointId">The EndPointID valid for this event</param>
        /// <param name="dateTime">The DateTime of this event being raised</param>
        /// <param name="sensorNumber">The Sensor Number of this event</param>
        /// <param name="NoData">Wether this contains valid data</param>
        /// <returns>Dictionary of alarms added</returns>
        public Dictionary<int, List<EventItem>> AddAlarmsAndValue(List<Alarm> alarms, int value, int endPointId, DateTime dateTime, int sensorNumber, bool NoData)
        {
            if (!NoData)
            {
                _logger.LogDebug($"Added alarms and value for sensor {sensorNumber}");
                var eventItem = new EventItem() { AlarmList = alarms.Where(o => o.SensorNumber == sensorNumber).ToList(), Value = value, EndPointId = endPointId, DateTime = dateTime, SensorNumber = sensorNumber };

                if (_eventItemDictionary.ContainsKey(sensorNumber))
                    _eventItemDictionary[sensorNumber].Add(eventItem);
                else
                    _eventItemDictionary.Add(sensorNumber, new List<EventItem>() { eventItem });
            }

            return _eventItemDictionary;
        }

        /// <summary>
        /// This will get the valid and triggered events from the events given
        /// </summary>
        /// <param name="Sensor">Which Sensor Events to look at</param>
        /// <returns>List Of Valid and Triggered Events</returns>
        public List<EventItem> GetEvents(int Sensor)
        {
            var newEvent = false;
            var orderedEvents = _eventItemDictionary[Sensor].OrderBy(o => o.DateTime).ToList();
            if (!_lastEvent.ContainsKey(Sensor))
            {
                newEvent = true;
                _lastEvent.Add(Sensor, new EventItem());
            }
            var listOfEvents = new List<EventItem>();

            foreach(var eventData in orderedEvents)
            {
                if (!eventData.TriggeredAlarms.Equals(_lastEvent[Sensor].TriggeredAlarms) || newEvent)
                {
                    newEvent = false;
                    listOfEvents.Add(eventData);
                    _lastEvent[Sensor] = eventData;
                }
            }

            return listOfEvents;
        }

        /// <summary>
        /// Fires the end Result method that will process all the alerts
        /// </summary>
        /// <returns>List of fired events</returns>
        public List<EventItem> EndResults()
        {
            var AllValidEvents = new List<EventItem>();
            if (_eventItemDictionary.Any())
            {
                var channelList = _database.GetMapper<EventItem>().GetFilteredResults<EventItem, EventItem>(_eventItemDictionary.First().Value.First());

                foreach (var Sensor in _eventItemDictionary)
                {
                    Sensor.Value.ForEach(o => o.AddChannelInfo(channelList.FirstOrDefault(x => x.SensorNumber == o.SensorNumber)));
                    var CriteriaEnteredList = GetEvents(Sensor.Key);
                    AllValidEvents.AddRange(CriteriaEnteredList);

                    foreach (var eventItem in CriteriaEnteredList)
                    {
                        _eventTargets.ForEach(o => o.send(eventItem, _lastEvent[Sensor.Key]));
                        var alarmsTriggered = eventItem.AlarmList.Where(x => eventItem.triggeredRules(x));
                        _logger.LogError($"Alarm has been met for channel number {eventItem.ChannelId} - senser number {eventItem.SensorId} - DateTime {eventItem.DateTime} - Value {eventItem.Value}");
                    }
                }
            }
            return AllValidEvents;
        }

        #endregion

        #region Disposers

        /// <summary>
        /// Run when Disposed to Fire off the events and clear the _eventDictionary
        /// </summary>
        public void Dispose()
        {
            EndResults();
            _eventItemDictionary.Clear();
        }

        #endregion
    }
}
